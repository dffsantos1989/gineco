<?php

return [
    'property_bulletin' => [
        'view_instructions' => 'Voir vos instructions de arriver',
        'intructions_sent' => 'Vos instructions d’enregistrement ont été envoyées à votre adresse e-mail.',
        'reasons' => 'Pourquoi dois-je remplir ce formulaire?',
        'deposit_reasons' => 'Pourquoi dois-je payer le dépôt de garantie?',
        'unique_id' => 'Numéro de réservation déjà enregistré',
        'privacy_policy' => 'Politique de confidentialité',
        'declare_agree' => "Je suis d'accord avec le",
        'declare_data_veracity' => 'Je déclare la vérité des données fournies. Je comprends la base juridique requise pour fournir les données incluses dans ce formulaire. Je comprends également que des documents peuvent me être demandés à tout moment pendant mon séjour à la propriété. ',
        'sucess_send_bulletin' => 'Envoyé avec succès!',
        'housing_bulletin' => 'Communication des invités',
        'accommodation' => 'le logement',
        'occupants_number' => 'Nombre de invités',
        'occupants' => 'Invités',
        'check_in' => 'Date darrivée',
        'check_out' => 'Date de départ',
        'name' => 'Le prénom',
        'last_name' => 'Le nom de famille',
        'birth_day' => 'Le jour de naissance',
        'identification_type' => "Sélectionnez votre type d'identifiant",
        'identification_number' => "Numéro d'identification",
        'country_origin' => "Sélectionnez votre pays d'origine",
        'country_residence' => 'Sélectionnez votre pays de résidence',
        'country_origin_identification' => "Sélectionnez votre identifiant de pays d'origine",
        'passport' => 'Passeport',
        'id_card' => 'Carte de citoyen',
        'other' => 'un autre',
        'amount' => 'Le montant',
        'pay_now' => 'Payer',
        'insert_card_n' => 'N ° de carte.',
        'succeeded' => 'Terminé',
        'security_deposit' => 'Dépôt de garantie',
        'book_id' => 'Nº de réservation ',
        'click_add_occupants' => "Cliquez pour ajouter le nombre d'invités",
        'marketing_agree' => 'Je souhaite recevoir des bons de réduction pour les réservations futures par le biais de communications marketing.',
    ],
];
