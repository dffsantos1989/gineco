<?php

namespace App\Form;

use App\Entity\AmountTranslation;
use App\Entity\Locales;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AmountTranslationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('name', TextType::class, [
                'label' => 'name',
                'required' => false,
                'attr' => ['class' => 'w3-input w3-border w3-white', 'placeholder' => 'name'],
            ])
            ->add('locales', EntityType::class, [
                'class' => Locales::class,
                'choice_label' => 'name',
                'placeholder' => false,
                'label' => false,
                'attr' => ['class' => 'w3-input w3-select w3-border w3-hide'],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => AmountTranslation::class,
        ]);
    }

    /*
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => BannerTranslation::class,
        ));
    }*/
}
