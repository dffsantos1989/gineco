<?php

namespace App\Form;

use App\Entity\PropertyDetails;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PropertyDetailsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        // ->add('property', HiddenType::class,
        // array(
        //     'data' => 'false',
        // ))
        ->add('abbreviation', TextType::class, [
            'label' => 'property_details.abbreviation',
            'required' => true,
            'attr' => [
                'class' => 'w3-input w3-border w3-white',
                'placeholder' => 'property_details.abbreviation',
                'pattern' => '/^[a-zA-Z0-9 ]{1,15}$/',
                'maxlength' => '15',
            ],
        ])
        ->add('address', TextType::class, [
            'label' => 'property_details.address',
            'required' => true,
            'attr' => [
                'class' => 'w3-input w3-border w3-white',
                'placeholder' => 'property_details.address',
                'maxlength' => '40',
            ],
        ])
        ->add('locality', TextType::class, [
            'label' => 'property_details.locality',
            'required' => true,
            'attr' => [
                'class' => 'w3-input w3-border w3-white',
                'placeholder' => 'property_details.locality',
                'pattern' => '/^[a-zçãáàéêíõôóúA-ZÇÃÁÀÉÊÍÕÔÓÚ][a-zçãáàéêíõôóúA-ZÇÃÁÀÉÊÍÕÔÓÚ’\- ]{1,30}$/',
                'maxlength' => '30',
            ],
        ])
        ->add('postalCode', TextType::class, [
            'label' => 'property_details.postal_code',
            'required' => true,
            'attr' => [
                'class' => 'w3-input w3-border w3-white',
                //'placeholder'=>'property_details.postal_code',
                'pattern' => '/^[0-9]{4}$/',
                'maxlength' => '4',
                'data-parsley-remote' => '',
                'data-parsley-remote-validator' => 'postal_code',
                ],
        ])
        ->add('postalZone', TextType::class, [
            'label' => 'property_details.postal_zone',
            'required' => true,
            'attr' => [
                'class' => 'w3-input w3-border w3-white',
                //'placeholder'=>'property_details.postal_zone',
                'pattern' => '/^[0-9]{3}$/',
                'maxlength' => '3',
            ],
        ])
        ->add('telephone', TextType::class, [
            'label' => 'property_details.telephone',
            'required' => true,
            'attr' => ['class' => 'w3-input w3-border w3-white', 'placeholder' => 'property_details.telephone',
            'pattern' => '/^[0-9]{9}$/',
            'maxlength' => '9',
        ],
        ])
        ->add('contactName', TextType::class, [
            'label' => 'property_details.contact_name',
            'required' => true,
            'attr' => ['class' => 'w3-input w3-border w3-white', 'placeholder' => 'property_details.contact_name',
            'maxlength' => '40',
            ],
        ])
        ->add('contactEmail', EmailType::class, [
            'label' => 'property_details.contact_email',
            'required' => true,
            'attr' => ['class' => 'w3-input w3-border w3-white', 'placeholder' => 'property_details.contact_email',
            'maxlength' => '140',
        ],
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PropertyDetails::class,
        ]);
    }

    public function getBlockPrefix()
    {
        return 'PropertyDetailsType';
    }
}
