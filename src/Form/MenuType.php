<?php

namespace App\Form;

use App\Entity\Menu;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MenuType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('active', CheckboxType::class, [
                'label' => 'active',
                'required' => false,
                'attr' => ['class' => 'w3-check'],
            ])
            ->add('isSubmenu', CheckboxType::class, [
                'label' => 'Submenu',
                'required' => false,
                'attr' => ['class' => 'w3-check'],
            ])
            ->add('superuser', CheckboxType::class, [
                'mapped' => false,
                'label' => 'SuperUser',
                'required' => false,
                'attr' => ['class' => 'w3-check'],
            ])
            ->add('admin', CheckboxType::class, [
                'mapped' => false,
                'label' => 'Admin',
                'required' => false,
                'attr' => ['class' => 'w3-check'],
            ])
            ->add('manager', CheckboxType::class, [
                'mapped' => false,
                'label' => 'Manager',
                'required' => false,
                'attr' => ['class' => 'w3-check'],
            ])
            ->add('icon', TextType::class, [
                'label' => 'Icon',
                'required' => false,
                'attr' => ['class' => 'w3-input w3-border w3-white', 'placeholder' => 'Icon'],
            ])
            ->add('path', TextType::class, [
                'label' => 'Path',
                'required' => false,
                'attr' => ['class' => 'w3-input w3-border w3-white', 'placeholder' => 'Path'],
            ])
            ->add('submit', SubmitType::class,
            [
                'label' => 'submit',
                'attr' => ['class' => 'w3-btn w3-block w3-border w3-green w3-margin-top'],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Menu::class,
        ]);
    }
}
