<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
// use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class ContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'attr' => ['class' => 'w3-input w3-padding-16 w3-border', 'placeholder' => 'contacts.name',
            ], 'required' => 'required', ])

            ->add('email', TextType::class, [
                'attr' => ['class' => 'w3-input w3-padding-16 w3-border', 'placeholder' => 'Email',
            ], 'required' => 'required', ])

            // ->add('phone', TextType::class, array(
            //     'attr'=> array( 'class' => 'form-control mb-30', 'placeholder' => 'phone'
            // ), 'required' => false ))

            // ->add('subject', TextType::class, array(
            //     'attr'=>array( 'class' => 'form-control mb-30', 'placeholder' => 'sub'
            // ),'required' => false ))

            ->add('message', TextareaType::class, [
                'attr' => ['class' => 'w3-input w3-padding-16 w3-border', 'placeholder' => 'contacts.message', 'rows' => '8', 'cols' => '80',
            ], 'required' => 'required', ])

            ->add('submit', SubmitType::class, ['label' => 'contacts.send',
                'attr' => ['class' => 'w3-button w3-black w3-padding-large',
            ], ]);
    }

    // public function configureOptions(OptionsResolver $resolver)
    // {
    //     $resolver->setDefaults(array(
    //         'data_class' => Seo::class,
    //     ));
    // }
}
