<?php

namespace App\Form;

use App\Entity\Admin;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdminType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        // ->add('address', TextType::class, array(
        //         'label'=>'address',
        //         'required' => true,
        //         'attr' => ['class' => 'w3-input w3-padding-16','placeholder'=>'address']
        //     ))
        //     ->add('location', TextType::class, array(
        //         'label'=>'location',
        //         'required' => true,
        //         'attr' => ['class' => 'w3-input w3-padding-16','placeholder'=>'location']
        //     ))
        //     ->add('postalCode', TextType::class, array(
        //         'label'=>'postal_code',
        //         'required' => true,
        //         'attr' => ['class' => 'w3-input w3-padding-16','placeholder'=>'postal_code']
        //     ))
        //     ->add('nif', TextType::class, array(
        //         'label'=>'NIF',
        //         'required' => true,
        //         'attr' => ['class' => 'w3-input w3-padding-16','placeholder'=>'NIF']
        //     ))
            ->add('email', EmailType::class,
            [
                'label' => 'email',
                'required' => true,
                'attr' => ['class' => 'w3-input w3-border w3-white', 'placeholder' => 'email'],
            ])
            ->add('username', TextType::class, [
                'label' => 'username',
                'required' => true,
                'attr' => ['class' => 'w3-input w3-border w3-white', 'placeholder' => 'username', 'autofocus' => 'autofocus'],
            ])
            ->add('plainPassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'first_options' => ['label' => 'Password *', 'attr' => ['class' => 'w3-input w3-padding-16', 'placeholder' => 'Password *', 'autocomplete' => 'new-password']],
                'second_options' => ['label' => 'Repetir Password *', 'attr' => ['class' => 'w3-input w3-padding-16', 'placeholder' => 'Repetir Password *', 'autocomplete' => 'new-password']],
                'attr' => ['autocomplete' => 'new-password'],
            ])
            ->add('submit', SubmitType::class,
            [
                'label' => 'submit',
                'attr' => ['class' => 'w3-btn w3-block w3-border w3-green w3-margin-top w3-hide'],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            //'data_class' => Admin::class,
        ]);
    }

    public function getBlockPrefix()
    {
        return 'AdminType';
    }
}
