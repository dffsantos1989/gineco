<?php

namespace App\Form;

use App\Entity\Locales;
use App\Entity\PropertyTranslation;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PropertyTranslationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        // ->add('property', HiddenType::class,
        // array(
        //     'data' => 'false',
        // ))
        // ->add('locales', HiddenType::class,
        // array(
        //     'data' => 'false',
        // ))

        ->add('reasons', TextareaType::class, [
            'label' => 'property_translation.reasons',
            'required' => false,
            'attr' => ['class' => 'w3-input w3-border w3-white ckeditor', 'placeholder' => 'property_translation.reasons'],
        ])
        ->add('instructions', TextareaType::class, [
            'label' => 'property_translation.instructions',
            'required' => false,
            'attr' => ['class' => 'w3-input w3-border w3-white ckeditor', 'placeholder' => 'property_translation.instructions'],
        ])
        ->add('locales', EntityType::class, [
            'class' => Locales::class,
            'choice_label' => 'filename',
            'placeholder' => false,
            'label' => false,
            'attr' => ['class' => 'w3-input w3-select w3-border w3-white locales w3-hide'],
        ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PropertyTranslation::class,
        ]);
    }

    public function getBlockPrefix()
    {
        return 'PropertyTranslationType';
    }
}
