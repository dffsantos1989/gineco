<?php

namespace App\Form;

use App\Entity\ProductDetails;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductDetailsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

         ->add('translation', CollectionType::class, [
                'entry_type' => ProductDetailsTranslationType::class,
                'entry_options' => ['label' => false],
                'required' => false,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'label' => false,
            ])

            // ->add('product_description_translation', CollectionType::class, array(
            //     'entry_type' => ProductDescriptionTranslationType::class,
            //     'entry_options' => array('label' => false),
            //     'allow_add' => true,
            //     'allow_delete' => true,
            //     'by_reference' => false,
            //     'label' => false
            // ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ProductDetails::class,
        ]);
    }

    public function getBlockPrefix()
    {
        return 'ProductDetailsType';
    }
}
