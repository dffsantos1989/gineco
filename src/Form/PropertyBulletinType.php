<?php

namespace App\Form;

use App\Entity\PropertyBulletin;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PropertyBulletinType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('bookId', TextType::class, [
            'label' => 'property_bulletin.book_id',
            'required' => true,
            'attr' => ['class' => 'w3-input  w3-border', 'placeholder' => 'property_bulletin.book_id', 'data-parsley-remote' => '', 'data-parsley-remote-validator' => 'bookid'],
        ])
        ->add('checkIn', DateType::class, [
            'widget' => 'single_text',
           'html5' => false,
           'format' => 'yyyy-MM-dd',
            'label' => 'property_bulletin.check_in',
            'required' => true,
            'attr' => ['class' => 'w3-input  w3-border',
            'placeholder' => 'property_bulletin.check_in',
            'readonly' => 'readonly',
            'data-parsley-trigger' => '',
            ],
        ])
        ->add('checkOut', DateType::class, [
            'widget' => 'single_text',
            'html5' => false,
            'format' => 'yyyy-MM-dd',
            'label' => 'property_bulletin.check_out',
            'required' => true,
            'attr' => ['class' => 'w3-input  w3-border', 'placeholder' => 'property_bulletin.check_out', 'readonly' => 'readonly'],
        ])
        ->add('guestNumber', IntegerType::class, [
            'label' => 'property_bulletin.occupants_number',
            'required' => true,
            'attr' => ['class' => 'add_guest_transmite_click w3-input w3-border', 'placeholder' => 'property_bulletin.occupants_number', 'title' => 'click_add_occupants', 'readonly' => 'readonly', 'value' => ''],
        ])

        ->add('guest', CollectionType::class, [
            'entry_type' => PropertyBulletinGuestType::class,
            'entry_options' => ['label' => false],
            'allow_add' => true,
            'allow_delete' => true,
            'label' => false,
            'required' => true,
            'by_reference' => false,
            'prototype' => true,
            'attr' => [
                'class' => 'my-selector',
            ],
        ])

        ->add('submit', SubmitType::class,
        [
            'label' => 'submit',
            'attr' => ['class' => 'w3-btn w3-block w3-border w3-green w3-section w3-hide'],
        ])

        ->add('marketing', HiddenType::class,
        [
            'data' => 'false',
        ])

        ->add('propertyId', HiddenType::class,
        [
            'mapped' => false,
            'data' => $options['property'],
        ])

        ->add('property', TextType::class,
        [
            'mapped' => false,
            'required' => false,
            'label' => 'property_bulletin.accommodation',
            'data' => $options['property_name'],
            'attr' => ['class' => 'title w3-block w3-border-0 w3-light-grey w3-text-blue', 'disabled' => 'disabled'],
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PropertyBulletin::class,
            'property' => null,
            'property_name' => null,
        ]);
    }

    private function options()
    {
        $c = [];

        for ($i = 1; $i <= 20; ++$i) {
            $c[$i] = true;
        }

        return $c;
    }

    public function getBlockPrefix()
    {
        return 'PropertyBulletinType';
    }
}
