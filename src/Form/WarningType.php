<?php

namespace App\Form;

use App\Entity\Warning;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class WarningType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('is_active', CheckboxType::class, [
                'label' => 'ative',
                'required' => false,
                'attr' => ['class' => 'w3-check'],
            ])
            ->add('submit', SubmitType::class,
            [
                'label' => 'save',
                'attr' => ['class' => 'w3-button w3-green w3-block w3-padding w3-section'],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Warning::class,
        ]);
    }
}
