<?php

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Company;
use App\Entity\Seo;
use App\Entity\Locales;

class RequestDBInfo
{
    private $projectDir;
    private $entityManager;

    public function __construct($projectDir, EntityManagerInterface $entityManager)
    {
        $this->projectDir = $projectDir;
        $this->entityManager = $entityManager;
    }
    
    public function getCompany()
    {
        $c = $this->entityManager->getRepository(Company::class)->find(1);
        return $c;
    }

    public function getSeo($request)
    {
        $s = $this->entityManager->getRepository(Seo::class)->findOneBy(['locales' => $this->defaultUserLocale($request)]);
        return $s;
    }

    private function defaultUserLocale($request)
    {
        $defaultUserLocale = 'en' == $request->getLocale() || 'en' == $request->getLocale() ? 'en' : 'pt';
        $userLocale = $this->entityManager->getRepository(Locales::class)->findOneBy(['name' => $defaultUserLocale]);
        return $userLocale;
    }
    
}
