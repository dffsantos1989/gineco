<?php

namespace App\Controller;

use App\Entity\Locales;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class TranslationController extends AbstractController
{
    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    public function translate($local = null)
    {
        $em = $this->getDoctrine()->getManager();

        $locales = $em->getRepository(Locales::class)->findOneBy(['name' => $local]);

        if (!$locales) {
            $locales = $em->getRepository(Locales::class)->findOneBy(['name' => 'en']);
        }

        $this->session->set('_locale', $locales->getName());

        return $this->redirectToRoute('index');
    }
}
