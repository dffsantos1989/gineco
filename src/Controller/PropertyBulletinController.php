<?php

namespace App\Controller;

use App\Entity\Admin;
use App\Entity\Company;
use App\Entity\Locales;
use App\Entity\Property;
use App\Entity\PropertyBulletin;
use App\Entity\Rgpd;
use App\Entity\SuperUser;
use App\Form\PropertyBulletinType;
use App\Service\Stripe;
use Doctrine\DBAL\DBALException;
use Dompdf\Dompdf;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
// Include Dompdf required namespaces
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class PropertyBulletinController extends AbstractController
{
    public function propertyBulletinNew($token, Stripe $stripe, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $locale = $request->getlocale();

        $property = $em->getRepository(Property::class)->findOneBy(['token' => $token]);
        $company = $em->getRepository(Company::class)->find(1);
        $locales = $em->getRepository(Locales::class)->findAll();
        $rgpd = $em->getRepository(Rgpd::class)->findOneBy(['locales' => $this->defaultUserLocale($request)]);

        $menu = new PropertyBulletin();
        $form = $this->createForm(PropertyBulletinType::class, $menu, ['property' => $property->getId(), 'property_name' => $property->getName()]);
        $form->handleRequest($request);

        $guestsMax = 20;

        // dd($property->getUser()->getEmail());
        // exit;

        return $this->render('admin/property-bulletin-new.html', [
            'form' => $form->createView(),
            'data' => $form->getData(),
            'locales' => $locales,
            'locale' => $locale,
            'property' => $property,
            'company' => $company,
            'payment_intent' => $stripe->createUpdatePaymentIntent($company, $request, $property, ''),
            'guestsMax' => $guestsMax,
            'rgpd' => $rgpd,
            'reasons' => $property->getCurrentTranslation($this->defaultUserLocale($request))['reasons'],
        ]);
    }

    public function validateField(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $result;

        $property = $em->getRepository(PropertyBulletin::class)->findBy(['book_id' => $request->query->get('PropertyBulletinType')['bookId']])
        ? $result = 0
        : $result = 1;

        return new Response($result);
    }

    public function bulletinsList(Request $request, ValidatorInterface $validator)
    {
        return $this->render('admin/bulletins-list.html', [
            ]
        );
    }

    public function bulletinsSearch(Request $request, ValidatorInterface $validator)
    {
        $em = $this->getDoctrine()->getManager();

        $start = $request->query->get('startDate') ? \DateTime::createFromFormat('d/m/Y', $request->query->get('startDate')) : null;
        $end = $request->query->get('endDate') ? \DateTime::createFromFormat('d/m/Y', $request->query->get('endDate')) : null;
        $property = $request->query->get('bulletins_property');
        $bookId = $request->query->get('bulletins_book_id');
        $loggedUser = $this->getUser();

        $user = ($loggedUser instanceof SuperUser || $loggedUser instanceof Admin) ? null : $loggedUser;
        $bulletins = $em->getRepository(PropertyBulletin::class)->getBulletinsByFilter($start, $end, $property, $bookId, $user);

        if ($bulletins) {
            $response = [
                'data' => $bulletins,
                'options' => count($bulletins),
                ];
        } else {
            $response = [
            'data' => '',
            'options' => 0,
        ];
        }

        return new JsonResponse($response);
    }

    public function propertyBulletinAdd(Request $request, TranslatorInterface $translator)
    {
        $propertyBulletin = new PropertyBulletin();

        $em = $this->getDoctrine()->getManager();

        $locale = $request->getlocale();

        $form = $this->createForm(PropertyBulletinType::class, $propertyBulletin);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            // dd($propertyBulletin);
            // exit;
            if ($form->isValid()) {
                $propertyBulletin = $form->getData();
                $property = $em->getRepository(Property::class)->findOneBy(['id' => $request->request->get('PropertyBulletinType')['propertyId']]);

                try {
                    $propertyBulletin->setProperty($property);
                    // dd($propertyBulletin->getGuest()[0]->getEmail());
                    // exit;
                    $em->persist($propertyBulletin);
                    $em->flush();

                    $company = $em->getRepository(Company::class)->find(1);
                    $t = $this->translations($translator, $locale);
                    $this->sendEmail($company, $propertyBulletin, $property, $t, $request);

                    $response = [
                            'status' => 1,
                            'message' => 'success',
                            'data' => $property->getCurrentTranslation($this->defaultUserLocale($request))['email_txt'],
                        ];
                } catch (DBALException $e) {
                    $a = ['Contate administrador sistema sobre: '.$e->getMessage()];
                    $response = [
                                'status' => 0,
                                'message' => 'fail',
                                'data' => $a,
                            ];
                }
            } else {
                $response = [
                        'status' => 0,
                        'message' => 'fail',
                        'data' => $this->getErrorMessages($form),
                    ];
            }
        } else {
            $response = [
                    'status' => 2,
                    'message' => 'fail not submitted',
                    'data' => '', ];
        }

        return new JsonResponse($response);
    }

    private function sendEmail($company, $bulletin, $property, $translations, $request)
    {
        $template = 'checkin_instructions';

        $transport = (new \Swift_SmtpTransport($company->getEmailSmtp(), $company->getEmailPort(), $company->getEmailCertificade()))
            ->setUsername($company->getEmail())
            ->setPassword($company->getEmailPass());

        $mailer = new \Swift_Mailer($transport);

        $message = (new \Swift_Message($property->getName().' - '.$translations['instructions']))
            ->setFrom([$company->getEmail() => $company->getName()])
            ->setBcc($bulletin->getGuest()[0]->getEmail())
            ->setTo([$bulletin->getGuest()[0]->getEmail() => $bulletin->getGuest()[0]->getName(), $property->getUser()->getEmail() => $property->getName()]) //$company->getEmail2() nauticdrive.fotos@gmail.com roman.bajireanu@intouchbiz.com
            ->addPart($translations['instructions'], 'text/plain')
            ->setBody(
                $this->renderView(
                   'emails/'.$template.'.twig',
                   ['company' => $company,
                   'logo' => $request->getHost().'/upload/gallery/'.$company->getLogo(),
                   'property' => ['instructions' => $property->getCurrentTranslation($this->defaultUserLocale($request))['email_txt'],
                   'name' => $property->getName(),
                    ],
                   'translations' => $translations,
                   ]
                ),
            'text/html'
        );

        $mailer->send($message);
    }

    private function translations($translator, $locale)
    {
        return ['hello' => $translator->trans('hello', [], 'messages', $locale),
                     'team' => $translator->trans('team', [], 'messages', $locale),
                     'instructions' => $translator->trans('property_bulletin.instructions', [], 'messages', $locale),
            ];
    }

    /**
     *Create Charge.
     *
     *@param $request
     *
     *@return json response of Request
     **/
    public function chargeCreditCard(Request $request, Stripe $stripe)
    {
        //$this->denyAccessUnlessGranted('ROLE_USER', null, 'Unable to access this page!');
        $id = $request->request->get('property_id');
        $bookId = $request->request->get('bookId');

        $em = $this->getDoctrine()->getManager();

        $company = $em->getRepository(Company::class)->find(1);
        $property = $em->getRepository(Property::class)->find($id);

        //$booking = $em->getRepository(Booking::class)->find($id);
        $i = $stripe->createUpdatePaymentIntent($company, $request, $property, $bookId);

        return 1 == $i['status'] ?
            new JsonResponse([
                'status' => 1,
                'message' => 'success',
                'data' => $i,
            ]) :
            new JsonResponse([
                'status' => 0,
                'message' => 'Unable to Charge Credit Card',
                'data' => null, ]);
    }

    protected function getErrorMessages(\Symfony\Component\Form\Form $form)
    {
        $errors = [];
        $err = [];
        foreach ($form->getErrors() as $key => $error) {
            $errors[] = $error->getMessage();
        }

        foreach ($form->all() as $child) {
            if (!$child->isValid()) {
                $errors[] = $this->getErrorMessages($child);
            }
        }

        foreach ($errors as $error) {
            $err[] = $error;
        }

        return $err;
    }

    private function defaultUserLocale(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $defaultUserLocale = 'en' == $request->getLocale() || 'en' == $request->getLocale() ? 'en' : 'pt';
        $userLocale = $em->getRepository(Locales::class)->findOneBy(['name' => $defaultUserLocale]);

        return $userLocale;
    }
}
