<?php

namespace App\Controller;

use App\Entity\AboutUs;
use App\Entity\Banner;
use App\Entity\Category;
use App\Entity\CategoryTranslation;
use App\Entity\Company;
use App\Entity\Gallery;
use App\Entity\Locales;
use App\Entity\Product;
use App\Entity\Rgpd;
use App\Entity\Seo;
use App\Entity\TermsConditions;
use App\Form\ContactType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\RequestDBInfo;

class HomeController extends AbstractController
{
    private $session;

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    public function index(Request $request, TranslatorInterface $translator, RequestDBInfo $dbinfo)
    {
        $em = $this->getDoctrine()->getManager();
        // $locale = $request->getlocale();
        $langCode = $this->langCode($this->defaultUserLocale($request));
        $landingPage = $this-> fetchItems($request, null, 12); //landing page
        $cs = $this-> fetchItems($request, null, 11); //Consultations
        $ex = $this-> fetchItems($request, null, 15); //Exams

        $banners = $em->getRepository(Banner::class)->findBy(array(), array('orderBy' => 'ASC'));
        foreach($banners as $banner) {
        	$bs[]  = ['image'=>$banner->getImage(),
        					'isActive'=>$banner->getIsActive(),
                            'textIsActive'=>$banner->getTextActive(),
                            'descriptionIsActive'=>$banner->getDescriptionActive(),
        					'data'=>$banner->getCurrentTranslation($this-> defaultUserLocale($request))];
        }

        return $this->render('home/index.twig', [
            'page' => 'index',
            'langCode' => $langCode,
            'landing_page' => $landingPage,
            'exams' => $ex,
            'consultations' => $cs,
            'company' => $dbinfo->getCompany(),
            'seo' => $dbinfo->getSeo($request),
            'banners'=>$bs,
        ]);
    }

    /**
     * @Route(
     *     "/{_locale}/consultations",
     *     name="consultations",
     *     requirements={
     *         "_locale": "%app_locales%"
     *     },
     *     defaults={
     *         "_locale": "%locale%"
     *     }
     * )
     */
    public function consultations(Request $request, TranslatorInterface $translator, RequestDBInfo $dbinfo)
    {
        $locale = $request->getlocale();
        $em = $this->getDoctrine()->getManager();
        $langCode = $this->langCode($this->defaultUserLocale($request));
        
        $consultations = $this->fetchItems($request, null, 11);//id is consultations

        // dd($consultations);
        // exit;

        return $this->render('home/consultations.twig', [
            'page' => 'consultations',
            'langCode' => $langCode,
            'consultations' => $consultations,
            'company' => $dbinfo->getCompany(),
            'seo' => $dbinfo->getSeo($request)
        ]);
    }

    /**
     * @Route(
     *     "/{_locale}/exams",
     *     name="exams",
     *     requirements={
     *         "_locale": "%app_locales%"
     *     },
     *     defaults={
     *         "_locale": "%locale%"
     *     }
     * )
     */
    public function exams(Request $request, TranslatorInterface $translator, RequestDBInfo $dbinfo)
    {
        $locale = $request->getlocale();
        $em = $this->getDoctrine()->getManager();
        $langCode = $this->langCode($this->defaultUserLocale($request));
        
        $exams = $this->fetchItems($request, null, 15);//id is exams

        return $this->render('home/exams.twig', [
            'page' => 'exams',
            'langCode' => $langCode,
            'exams' => $exams,
            'company' => $dbinfo->getCompany(),
            'seo' => $dbinfo->getSeo($request)
        ]);
    }

    /**
     * @Route(
     *     "/{_locale}/exams_list/{id}/{text}",
     *     name="exams_list",
     *     requirements={
     *         "_locale": "%app_locales%"
     *     },
     *     defaults={
     *         "_locale": "%locale%"
     *     }
     * )
     */
    public function exams_list(Request $request, RequestDBInfo $dbinfo, $id)
    {
        $locale = $request->getlocale();
        $em = $this->getDoctrine()->getManager();
        $langCode = $this->langCode($this->defaultUserLocale($request));
        
        
        $exams_list = $this->fetchItem($request, 'id', $id);

        // dd($services);
        // exit;

        return $this->render('home/exams-list.twig', [
            'page' => 'exams_list',
            'langCode' => $langCode,
            'exams_list' => $exams_list,
            
            'company' => $dbinfo->getCompany(),
            'seo' => $dbinfo->getSeo($request)
        ]);
    }

    /**
     * @Route(
     *     "/{_locale}/other_services",
     *     name="other_services",
     *     requirements={
     *         "_locale": "%app_locales%"
     *     },
     *     defaults={
     *         "_locale": "%locale%"
     *     }
     * )
     */
    public function other_services(Request $request, TranslatorInterface $translator, RequestDBInfo $dbinfo)
    {
        $locale = $request->getlocale();
        $em = $this->getDoctrine()->getManager();
        $langCode = $this->langCode($this->defaultUserLocale($request));

        
        
        $other_services = $this->fetchItems($request, null, 13); //other_services

        return $this->render('home/other_services.twig', [
            'page' => 'other_services',
            'langCode' => $langCode,
            'other_services' => $other_services,
            
            'company' => $dbinfo->getCompany(),
            'seo' => $dbinfo->getSeo($request)
        ]);
    }

    /**
     * @Route(
     *     "/{_locale}/gallery",
     *     name="gallery",
     *     requirements={
     *         "_locale": "%app_locales%"
     *     },
     *     defaults={
     *         "_locale": "%locale%"
     *     }
     * )
     */
    public function gallery(Request $request, TranslatorInterface $translator, RequestDBInfo $dbinfo)
    {
        $locale = $request->getlocale();
        $em = $this->getDoctrine()->getManager();
        $langCode = $this->langCode($this->defaultUserLocale($request));
        
        $fd = $this-> fetchItems($request, null, 10); //gallery

        return $this->render('home/gallery.twig', [
            'page' => 'gallery',
            'langCode' => $langCode,
            
            
            'company' => $dbinfo->getCompany(),
            'seo' => $dbinfo->getSeo($request)
        ]);
    }

    /**
     * @Route(
     *     "/{_locale}/consultations_list/{id}/{text}",
     *     name="consultations_list",
     *     requirements={
     *         "_locale": "%app_locales%"
     *     },
     *     defaults={
     *         "_locale": "%locale%"
     *     }
     * )
     */
    public function consultations_list(Request $request, RequestDBInfo $dbinfo, $id)
    {
        $locale = $request->getlocale();
        $em = $this->getDoctrine()->getManager();
        $langCode = $this->langCode($this->defaultUserLocale($request));
        
        
        $consultations_list = $this->fetchItem($request, 'id', $id);

        // dd($services);
        // exit;

        return $this->render('home/consultations-list.twig', [
            'page' => 'consultations_list',
            'langCode' => $langCode,
            'consultations_list' => $consultations_list,
            
            'company' => $dbinfo->getCompany(),
            'seo' => $dbinfo->getSeo($request)
        ]);
    }

    /**
     * @Route(
     *     "/{_locale}/about",
     *     name="about",
     *     requirements={
     *         "_locale": "%app_locales%"
     *     },
     *     defaults={
     *         "_locale": "%locale%"
     *     }
     * )
     */
    public function about(Request $request, TranslatorInterface $translator, RequestDBInfo $dbinfo)
    {
        $locale = $request->getlocale();
        $em = $this->getDoctrine()->getManager();
        $langCode = $this->langCode($this->defaultUserLocale($request));
        
        $about = $em->getRepository(AboutUs::class)->findOneBy(['locales' => $this->defaultUserLocale($request)]);
        
        return $this->render('home/about.twig', [
            'page' => 'about',
            'about' => $about,
            'langCode' => $langCode,
            
            'company' => $dbinfo->getCompany(),
            'seo' => $dbinfo->getSeo($request)
        ]);
    }
    /**
     * @Route(
     *     "/{_locale}/contact",
     *     name="contact",
     *     requirements={
     *         "_locale": "%app_locales%"
     *     },
     *     defaults={
     *         "_locale": "%locale%"
     *     }
     * )
     */
    public function contact(Request $request, TranslatorInterface $translator, RequestDBInfo $dbinfo)
    {
        $locale = $request->getlocale();
        $em = $this->getDoctrine()->getManager();

        $langCode = $this->langCode($this->defaultUserLocale($request));
        

        return $this->render('home/contact.twig', [
            'page' => 'contact',
            'langCode' => $langCode,
            //'services' => $services,
            
            'company' => $dbinfo->getCompany(),
            'seo' => $dbinfo->getSeo($request)
        ]);
    }

    public function navbarExams(Request $request) {
        $ex = $this-> fetchItems($request, null, 15); //Exams

        return  $this->render('home/navbar-exams.html.twig',[
            'exams' => $ex,
        ]);
    }

    public function navbarConsultations(Request $request) {
        $cs= $this-> fetchItems($request, null, 11); //Consultations

        return  $this->render('home/navbar-consultations.html.twig',[
            'consultations' => $cs,
        ]);
    }

    public function socialMediaIcons(RequestDBInfo $dbinfo) {
        $socialNetworkIcons = $this->fileFinder('../public_html/images/socialMedia/');

        return  $this->render('home/social-network-icons.html.twig',[
            'social_network_icons' => $socialNetworkIcons,
            'company' => $dbinfo->getCompany(),
        ]);
    }

    public function sendEmail(Request $request, \Swift_Mailer $mailer, TranslatorInterface $translator, RequestDBInfo $dbinfo)
    {
        $c = $dbinfo->getCompany();
        $err = [];
        $locale = $request->getlocale();
        //IF FIELDS IS NULL PUT IN ARRAY AND SEND BACK TO USER
        $request->request->get('name') ? $name = $request->request->get('name') : $err[] = 'name';
        $request->request->get('email') ? $email = $request->request->get('email') : $err[] = 'email';
        $request->request->get('phone') ? $telephone = $request->request->get('phone') : $err[] = 'phone';
        $request->request->get('subject') ? $subject = $request->request->get('subject') : $err[] = 'subject';
        $request->request->get('msg') ? $information = $request->request->get('msg') : $err[] = 'msg';

        if ($err) {
            $response = [
                'status' => 0,
                'message' => 'fields empty',
                'data' => $err,
                'mail' => null,
                'locale' => $locale,
            ];

            return new JsonResponse($response);
        }

        //NO FAKE DATA
        1 == $this->noFakeName($name) ? $err[] = 'contact_name' : false;
        1 == $this->noFakeEmails($email) ? $err[] = 'contact_email' : false;
        1 == $this->noFakeTelephone($telephone) ? $err[] = 'contact_telephone' : false;

        if ($err) {
            $response = [
                'status' => 2,
                'message' => 'invalid fields',
                'data' => $err,
                'mail' => null,
                'locale' => $locale,
            ];

            return new JsonResponse($response);
        } else {
            $fields = ['name' => $name, 'email' => $email, 'message' => $information];

            $translations = $this->emailTranslation($translator, $locale);

            $transport = (new \Swift_SmtpTransport($c->getEmailSmtp(), $c->getEmailPort(), $c->getEmailCertificade()))
                    ->setUsername($c->getEmail())
                    ->setPassword($c->getEmailPass());

            $mailer = new \Swift_Mailer($transport);

            $subject = $request->request->get('subject'); //$translations["info_request"];

            $message = (new \Swift_Message($subject))
                    ->setFrom([$c->getEmail() => $c->getName()])
                    ->setTo([$email => $name, $c->getEmail() => $c->getName()])
                    ->addPart($subject, 'text/plain')
            ->setBody(
                $this->renderView(
                'emails/contact.twig', ['company' => $c, 'fields' => $fields, 'translations' => $translations,
                'logo' => $request->getHost().'/upload/logo/logo.png', ]
                ),
                'text/html'
            );
            $send = $mailer->send($message);
        }

        $response = [
                'status' => 1,
                'message' => 'all valid',
                'data' => 'success',
                'mail' => $send,
                'locale' => $locale,
        ];

        return new JsonResponse($response);
    }

    private function emailTranslation($translator, $locale)
    {
        return ['hello' => $translator->trans('hello', [], 'messages', $locale),
                'name' => $translator->trans('name', [], 'messages', $locale),
                'email' => $translator->trans('email', [], 'messages', $locale),
                'telephone' => $translator->trans('telephone', [], 'messages', $locale),
                'locality' => $translator->trans('locality', [], 'messages', $locale),
                'message' => $translator->trans('message', [], 'messages', $locale),
                'team' => $translator->trans('team', [], 'messages', $locale),
                'asking_info' => $translator->trans('asking_info', [], 'messages', $locale),
                'request_information' => $translator->trans('request_information', [], 'messages', $locale),
                'info_request' => $translator->trans('contacts.info_request', [], 'messages', $locale),
                'subject' => $translator->trans('subject', [], 'messages', $locale),
        ];
    }

    public function userTranslation($lang, $page)
    {
        $this->session->set('_locale', $lang);

        return $this->redirectToRoute($page, ['_locale' => $lang]);
    }

    private function defaultUserLocale(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $defaultUserLocale = 'en' == $request->getLocale() || 'en' == $request->getLocale() ? 'en' : 'pt';
        $userLocale = $em->getRepository(Locales::class)->findOneBy(['name' => $defaultUserLocale]);

        return $userLocale;
    }

    private function noFakeEmails($email)
    {
        $invalid = 0;
        if ($email) {
            $validator = new \EmailValidator\Validator();
            $validator->isEmail($email) ? false : $invalid = 1;
            $validator->isSendable($email) ? false : $invalid = 1;
            $validator->hasMx($email) ? false : $invalid = 1;
            null != $validator->hasMx($email) ? false : $invalid = 1;
            $validator->isValid($email) ? false : $invalid = 1;
        }

        return $invalid;
    }

    private function noFakeName($a)
    {
        $invalid = 0;
        if ($a) {
            $invalid = preg_replace("/[^!@#\$%\^&\*\(\)\[\]:;]/", '', $a);
        }

        return $invalid;
    }

    private function noFakeTelephone($a)
    {
        $invalid = 0;
        if ($a) {
            $invalid = preg_replace("/[0-9|\+?]{0,2}[0-9]{5,12}/", '', $a);
        }

        return $invalid;
    }

    private function langCode($locales)
    {
        switch ($locales->getName()) {
            case 'pt': return 'pt';
            break;
            case 'en':return 'en';
            break;
            default:
                        return 'x-default';
        }
    }

    private function fileFinder($dir)
    {
        $icons_finder = new Finder();
        $icons_finder->files()->in($dir)->sortByName();
        $icons_name = [];
        foreach ($icons_finder as $name) {
            array_push($icons_name, basename($name));
        }

        return $icons_name;
    }

    private function fetchItems(Request $request, $highlight, $category)
    {
        $em = $this->getDoctrine()->getManager();
        // $products = $em->getRepository(Product::class)->findBy([$key => $value],['orderBy' => 'ASC']);
        $category = $em->getRepository(Category::class)->findOneBy(['id' => $category]);
        $products = $em->getRepository(Product::class)->findBySomething($highlight, $category);

        foreach ($products as $product) {
            $myAmounts = [];
            $myDetails = [];
            //$myCategories = [];

            foreach ($product->getAmount() as $amounts) {
                $myAmounts[] = ['id' => $amounts->getId(), 'isActive' => $amounts->getIsActive(), 'amount' => $amounts->getAmount()->getAmount(), 'currency' => $amounts->getAmount()->getCurrency()->getCode(), 'name' => $amounts->getCurrentTranslation($this->defaultUserLocale($request))];
            }

            foreach ($product->getDetail() as $details) {
                $myDetails = ['id' => $details->getId(), 'detail' => $details->getCurrentTranslation($this->defaultUserLocale($request))];
            }

            $myCategories = ['id' => $product->getCategory()->getId(), 'isActive' => $product->getCategory()->getIsActive(), 'name' => $product->getCategory()->getCurrentTranslation($this->defaultUserLocale($request))];

            $p[] = [
                'id' => $product->getId(),
                'highlight' => $product->getHighlight(),
                'isActive' => $product->getIsActive(),
                'title' => $product->getCurrentTranslationName($this->defaultUserLocale($request)),
                'description' => $product->getCurrentTranslationHtml($this->defaultUserLocale($request)),
                'checkInText' => $product->getCurrentTranslationCheckInText($this->defaultUserLocale($request)),
                'seoDescription' => $product->getCurrentTranslationSeoDescription($this->defaultUserLocale($request)),
                'image' => $product->getImage(),
                'amounts' => $myAmounts,
                'details' => $myDetails,
                'category' => $myCategories,
            ];
        }
        
        return $p;
    }

    private function fetchItem(Request $request, $key, $value)
    {
        $em = $this->getDoctrine()->getManager();
        $products = $em->getRepository(Product::class)->findBy([$key => $value], ['orderBy' => 'ASC']);
        foreach ($products as $product) {
            $myAmounts = [];
            $myDetails = [];
            foreach ($product->getAmount() as $amounts) {
                $myAmounts[] = ['id' => $amounts->getId(),  'isActive' => $amounts->getIsActive(), 'amount' => $amounts->getAmount()->getAmount(), 'currency' => $amounts->getAmount()->getCurrency()->getCode(), 'name' => $amounts->getCurrentTranslation($this->defaultUserLocale($request))];
            }

            foreach ($product->getDetail() as $details) {
                $myDetails[] = ['id' => $details->getId(), 'detail' => $details->getCurrentTranslation($this->defaultUserLocale($request))];
            }

            $p = [
                'id' => $product->getId(),
                'title' => $product->getCurrentTranslationName($this->defaultUserLocale($request)),
                'description' => $product->getCurrentTranslationHtml($this->defaultUserLocale($request)),
                'checkInText' => $product->getCurrentTranslationCheckInText($this->defaultUserLocale($request)),
                'seoDescription' => $product->getCurrentTranslationSeoDescription($this->defaultUserLocale($request)),
                'image' => $product->getImage(),
                'amounts' => $myAmounts,
                'details' => $myDetails,
            ];
        }

        return $p;
    }

    public function sidebar(Request $request)
    {
 
        $sidebar = $this-> fetchItems($request, null, 14); //sidebar
       
        return $this->render(
            'home/sidebar.twig',
            [ 'sidebar'=>$sidebar,
            ]
        );
    }

    protected function getErrorMessages(\Symfony\Component\Form\Form $form)
    {
        $errors = [];
        $err = [];
        foreach ($form->getErrors() as $key => $error) {
            $errors[] = $error->getMessage();
        }

        foreach ($form->all() as $child) {
            if (!$child->isValid()) {
                $errors[] = $this->getErrorMessages($child);
            }
        }

        foreach ($errors as $error) {
            $err[] = $error;
        }

        return $err;
    }
}
