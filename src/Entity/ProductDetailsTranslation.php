<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="product_details_translation")
 * @ORM\Entity(repositoryClass="App\Repository\ProductDetailsTranslationRepository")
 */
class ProductDetailsTranslation
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $name;
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $text;

    /** @ORM\ManyToOne(targetEntity="ProductDetails", inversedBy="translation") */
    private $product_detail;

    /** @ORM\ManyToOne(targetEntity="Locales") */
    private $locales;

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = str_replace("'", '’', $name);
    }

    public function getText()
    {
        return $this->text;
    }

    public function setText($text)
    {
        $this->text = str_replace("'", '’', $text);
    }

    public function getLocales()
    {
        return $this->locales;
    }

    public function setLocales(Locales $locales)
    {
        $this->locales = $locales;
    }

    public function getProductDetail()
    {
        return $this->product_detail;
    }

    public function setProductDetail(ProductDetails $product_detail)
    {
        $this->product_detail = $product_detail;
    }
}
