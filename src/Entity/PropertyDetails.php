<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *  * @ORM\Table(name="property_details")
 * @ORM\Entity(repositoryClass="App\Repository\PropertyDetailsRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class PropertyDetails
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", name="abbreviation")
     */
    private $abbreviation;

    /**
     * @ORM\Column(type="string", name="address")
     */
    private $address;

    /**
     * @ORM\Column(type="string", name="locality")
     */
    private $locality;

    /**
     * @ORM\Column(type="string", name="postal_code")
     */
    private $postal_code;

    /**
     * @ORM\Column(type="string", name="postal_zone")
     */
    private $postal_zone;

    /**
     * @ORM\Column(type="string", name="telephone")
     */
    private $telephone;

    /**
     * @ORM\Column(type="string", name="fax", nullable=true)
     */
    private $fax;

    /**
     * @ORM\Column(type="string", name="contact_name")
     */
    private $contact_name;

    /**
     * @ORM\Column(type="string", name="contact_email")
     */
    private $contact_email;

    /** @ORM\ManyToOne(targetEntity="Property", inversedBy="details") */
    private $property;

    public function getId()
    {
        return $this->id;
    }

    public function getProperty()
    {
        return $this->property;
    }

    public function setProperty(Property $property)
    {
        $this->property = $property;
    }

    public function getAbbreviation()
    {
        return $this->abbreviation;
    }

    public function setAbbreviation($abbreviation)
    {
        $this->abbreviation = $abbreviation;
    }

    public function getAddress()
    {
        return $this->address;
    }

    public function setAddress($address)
    {
        $this->address = $address;
    }

    public function getLocality()
    {
        return $this->locality;
    }

    public function setLocality($locality)
    {
        $this->locality = $locality;
    }

    public function getPostalCode()
    {
        return $this->postal_code;
    }

    public function setPostalCode($postal_code)
    {
        $this->postal_code = $postal_code;
    }

    public function getPostalZone()
    {
        return $this->postal_zone;
    }

    public function setPostalZone($postal_zone)
    {
        $this->postal_zone = $postal_zone;
    }

    public function getTelephone()
    {
        return $this->telephone;
    }

    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;
    }

    public function getFax()
    {
        return $this->fax;
    }

    public function setFax($fax)
    {
        $this->fax = $fax;
    }

    public function getContactName()
    {
        return $this->contact_name;
    }

    public function setContactName($contact_name)
    {
        $this->contact_name = $contact_name;
    }

    public function getContactEmail()
    {
        return $this->contact_email;
    }

    public function setContactEmail($contact_email)
    {
        $this->contact_email = $contact_email;
    }
}
