<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PropertyBulletinGuestRepository")
 */
class PropertyBulletinGuest
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", name="name")
     */
    private $name;

    /**
     * @ORM\Column(type="string", name="last_name")
     */
    private $last_name;

    /** @ORM\Column(type="datetime", name="birth_date")
     *
     */
    private $birth_date;

    /**
     * @ORM\Column(type="string", name="birth_place", nullable=true)
     */
    private $birth_place;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=100, name="telephone")
     */
    private $telephone;

    /**
     * @ORM\Column(type="string", name="identification_type")
     */
    private $identification_type;
    /**
     * @ORM\Column(type="string", name="identification_number")
     */
    private $identification_number;
    /**
     *@ORM\ManyToOne(targetEntity="Countries")
     *country_origin
     */
    private $country_origin;

    /**
     *@ORM\ManyToOne(targetEntity="Countries")
     * @Assert\NotBlank(message="country_residence")
     */
    private $country_residence;

    /**
     *@ORM\ManyToOne(targetEntity="Countries")
     * @Assert\NotBlank(message="country_origin_identification")
     */
    private $country_origin_identification;

    /**
     * @ORM\Column(type="string", name="country_origin_place", nullable=true)
     */
    private $country_origin_place;

    /**
     * @ORM\ManyToOne(targetEntity="PropertyBulletin", inversedBy="guest") */
    private $property_bulletin;

    public function getPropertyBulletin()
    {
        return $this->property_bulletin;
    }

    public function setPropertyBulletin(propertyBulletin $property_bulletin)
    {
        $this->property_bulletin = $property_bulletin;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = str_replace("'", '’', $email);
    }

    public function getTelephone()
    {
        return $this->telephone;
    }

    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getLastName()
    {
        return $this->last_name;
    }

    public function setLastName($last_name)
    {
        $this->last_name = $last_name;
    }

    public function getBirthDate()
    {
        return $this->birth_date;
    }

    public function setBirthDate($birth_date)
    {
        $this->birth_date = $birth_date;
    }

    public function getIdentificationType()
    {
        return $this->identification_type;
    }

    public function setIdentificationType($identification_type)
    {
        $this->identification_type = $identification_type;
    }

    public function getGuestNumber()
    {
        return $this->guest_number;
    }

    public function setGuestNumber($guest_number)
    {
        $this->guest_number = $guest_number;
    }

    public function getIdentificationNumber()
    {
        return $this->identification_number;
    }

    public function setIdentificationNumber($identification_number)
    {
        $this->identification_number = $identification_number;
    }

    public function getCountryOrigin()
    {
        return $this->country_origin;
    }

    public function setCountryOrigin(Countries $country_origin)
    {
        $this->country_origin = $country_origin;
    }

    public function getCountryResidence()
    {
        return $this->country_residence;
    }

    public function setCountryResidence(Countries $country_residence)
    {
        $this->country_residence = $country_residence;
    }

    public function getCountryOriginIdentification()
    {
        return $this->country_origin_identification;
    }

    public function setCountryOriginIdentification(Countries $country_origin_identification)
    {
        $this->country_origin_identification = $country_origin_identification;
    }
}
