<?php

namespace App\Repository;

use App\Entity\PropertyBulletin;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class PropertyBulletinRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PropertyBulletin::class);
    }

    public function getBulletinsByFilter($start, $end, $name, $bookId, $user)
    {
        $start = $start ? new \DateTime($start->format('Y-m-d')) : $start;
        $end = $end ? new \DateTime($end->format('Y-m-d')) : $end;
        $name = '' == $name ? null : $name;
        $bookId = '' == $bookId ? null : $bookId;

        $dql = 'SELECT b.book_id,  c_o.iso3 as country_origin, c_r.iso3 as country_residence,  c_o_i.iso3 as country_origin_identification, g.name, g.last_name, 
        DATE_FORMAT(g.birth_date,\'%Y-%m-%d\') as birth_date, g.email, g.telephone, g.identification_number, g.identification_type, b.marketing,
        DATE_FORMAT(b.check_in,\'%Y-%m-%d\') as check_in, DATE_FORMAT(b.check_out,\'%Y-%m-%d\') as check_out, p.name as property
            FROM App\Entity\PropertyBulletin b
            JOIN b.property p
            JOIN b.guest g
            JOIN g.country_origin c_o
            JOIN g.country_residence c_r
            JOIN g.country_origin_identification c_o_i
            WHERE (b.check_in >= :start OR :start IS NULL)
            AND (b.check_out <= :end  OR :end IS NULL)
            AND (p.name = :name OR :name IS NULL) 
            AND (b.book_id = :bookId  OR :bookId IS NULL)
            AND (p.user = :user  OR :user IS NULL)   
        ';

        $query = $this->getEntityManager()->createQuery($dql)
        ->setParameter('start', $start)
        ->setParameter('end', $end)
        ->setParameter('name', $name)
        ->setParameter('bookId', $bookId)
        ->setParameter('user', $user);

        if (!$start and !$end and !$name and !$bookId) {
            return [];
        }

        return $query->execute();
    }

    public function getBulletins()
    {
        $now = new \DateTime('now');
        $now = new \DateTime($now->format('Y-m-d').' 00:00:00');
        $tomorow = new \DateTime($now->format('Y-m-d').' 00:00:00');
        $tomorow->modify('+1 day');

        //, p.name as pNome, p.hotel_unit_code as Codigo_Unidade_Hoteleira,
        // p.establishment as Estabelecimento, p.sef_api_key, pd.abbreviation as Abreviatura, pd.address as Morada,
        // pd.locality as Localidade, pd.postal_code as Codigo_Postal, pd.postal_zone as Zona_Postal,
        // pd.telephone as Telefone, pd.fax as Fax, pd.contact_name as Nome_Contacto, pd.contact_email as Email_Contacto

        //$dql = 'SELECT b,g,p,c_o,c_r,c_o_i,p,pd
        $dql = 'SELECT g.last_name as Apelido, g.name as Nome, c_o.iso3 as Nacionalidade,
        DATE_FORMAT(g.birth_date,\'%Y-%m-%dT%H:%i:%s\') as Data_Nascimento, g.birth_place as Local_Nascimento, g.identification_number as Documento_Identificacao,
        c_r.iso3 as Pais_Emissor_Documento, g.identification_type as Tipo_Documento,
        DATE_FORMAT(b.check_in,\'%Y-%m-%dT%H:%i:%s\') as Data_Entrada, DATE_FORMAT(b.check_out,\'%Y-%m-%dT%H:%i:%s\') as Data_Saida, c_o_i.iso3 as Pais_Residencia_Origem, 
        g.country_origin_place as Local_Residencia_Origem, p.id
        FROM App\Entity\PropertyBulletin b
        JOIN b.guest g
        JOIN b.property p
        JOIN p.details as pd
        JOIN g.country_origin c_o
        JOIN g.country_residence c_r
        JOIN g.country_origin_identification c_o_i
        WHERE b.check_in = :now 
        AND b.check_in < :tomorow';

        $query = $this->getEntityManager()->createQuery($dql)
            ->setParameter('now', $now)
            ->setParameter('tomorow', $tomorow);

        $r = $query->getResult();

        $guests = [];
        foreach ($r as $key => $value) {
            $id = $value['id'];
            unset($value['id']);
            if (isset($guests[$id])) {
                $guests[$id][] = $value;
            } else {
                $guests[$id] = [$value];
            }
        }

        $bulletins = [];

        foreach ($guests as $key => $value) {
            $bulletins[] = [
                'Unidade_Hoteleira' => $this->getPropertyDetails($key),
                'Boletim_Alojamento' => $value,
                'Envio' => [
                    'Numero_Ficheiro' => sizeof($this->getSentBulletins($key)) + 1, // +1 is an increment of the current number of bulletin to sent related to a property
                    'Data_Movimento' => (new \DateTime('now'))->format('Y-m-d\TH:i:s'),
                ],
            ];
        }

        return $bulletins;
    }

    public function getSentBulletins($propertyID)
    {
        $now = new \DateTime('now');
        $now = new \DateTime($now->format('Y-m-d').' 00:00:00');

        $dql = 'SELECT COUNT(p) 

        FROM App\Entity\PropertyBulletin b
        JOIN b.property p
        WHERE b.check_in < :now
        AND p.id = :propertyID 
        ';

        $query = $this->getEntityManager()->createQuery($dql)
            ->setParameter('propertyID', $propertyID)
            ->setParameter('now', $now);

        return $query->getOneOrNullResult();
    }

    public function getPropertyDetails($propertyID)
    {
        $dql = 'SELECT p.hotel_unit_code as Codigo_Unidade_Hoteleira, p.establishment as Estabelecimento, p.name as Nome,
                d.abbreviation as Abreviatura, d.address as Morada, d.locality as Localidade,
                d.postal_code as Codigo_Postal, d.postal_zone as Zona_Postal, d.telephone as Telefone,
                d.fax as Fax, d.contact_name as Nome_Contacto, d.contact_email as Email_Contacto,
                p.sef_api_key as Chave_Acesso  

        FROM App\Entity\PropertyDetails d
        JOIN d.property p
        WHERE p.id = :propertyID 
        ';

        $query = $this->getEntityManager()->createQuery($dql)
            ->setParameter('propertyID', $propertyID);

        return $query->getOneOrNullResult();
    }

    public function findProperty($property)
    {
        return $this->createQueryBuilder('p')
        ->where('p = :property')
        ->setParameter('property', $property);
    }
}
